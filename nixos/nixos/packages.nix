{ config, pkgs, ... }:

{
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    alacritty
    appimage-run
    bat
    bitwarden
    borgbackup
    cifs-utils
    clang-tools_15
    cmake
    cudaPackages_10.cudatoolkit
    difftastic
    direnv
    discord
    docker
    docker-compose
    du-dust
    duf
    fd
    feh
    firefox
    fish
    fzf
    gcc
    git
    gnumake
    gnupg
    helix
    htop
    i3lock
    killall
    lazygit
    lldb
    neovim
    nvidia-docker
    obsidian
    pass
    pinentry
    pinentry-gtk2
    playerctl
    pulseaudio
    python310
    qutebrowser
    redshift
    ripgrep
    rofi
    spotify
    stow
    tig
    tmux
    tree
    wget
    xclip
  ];

}
