let b:ale_linters = ['verilator']
if !empty($VERILATOR_INCLUDES)
    let b:ale_verilog_verilator_options = '-sv -y $VERILATOR_INCLUDES'
else
    let b:ale_verilog_verilator_options = '-sv'
endif
