let b:ale_python_pycodestyle_options = "--max-line-length=100"
let b:ale_python_mypy_options = "--ignore-missing-imports"
let b:ale_fixers = ['black', 'isort', 'trim_whitespace']
let b:ale_linters = ['ruff']

nnoremap <leader>dt :lua require('dap-python').test_method()<CR>
