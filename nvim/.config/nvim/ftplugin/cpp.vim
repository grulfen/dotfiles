set tabstop=2
set shiftwidth=2

let b:ale_fixers = ['clang-format', 'trim_whitespace']
let b:ale_linters = []

setlocal makeprg=cmake\ --build\ build
