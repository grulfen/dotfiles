" Vim filetype plugin for using emacs verilog-mode
" Last Change: 2007 August 29
" Maintainer:  Seong Kang <seongk@wwcoms.com>
" License:     This file is placed in the public domain.

if exists("loaded_verilog_emacsauto")
   finish
endif
let loaded_verilog_emacsauto = 1

if !hasmapto('<Plug>VerilogEmacsAutoAdd')
   map <unique> <Leader>va <Plug>VerilogEmacsAutoAdd
endif
if !hasmapto('<Plug>VerilogEmacsAutoDelete')
   map <unique> <Leader>vd <Plug>VerilogEmacsAutoDelete
endif
if !hasmapto('<Plug>VerilogEmacsAutoInject')
   map <unique> <Leader>vi <Plug>VerilogEmacsAutoInject
endif
if !hasmapto('<Plug>VerilogEmacsAutoIndent')
   map <unique> <Leader>vt <Plug>VerilogEmacsAutoIndent
endif

command! VerilogAutoAdd call s:Add()
command! VerilogAutoDelete call s:Delete()
command! VerilogAutoInject call s:Inject()
command! VerilogAutoIndent call s:Indent()

noremap <unique> <script> <Plug>VerilogEmacsAutoAdd    <SID>Add
noremap <unique> <script> <Plug>VerilogEmacsAutoDelete <SID>Delete
noremap <unique> <script> <Plug>VerilogEmacsAutoInject <SID>Inject
noremap <unique> <script> <Plug>VerilogEmacsAutoIndent <SID>Indent
noremap <SID>Add    :call <SID>Add()<CR>
noremap <SID>Delete :call <SID>Delete()<CR>
noremap <SID>Inject :call <SID>Inject()<CR>
noremap <SID>Indent :call <SID>Indent()<CR>

" Add function
function s:Add()
   if &expandtab
      let s:save_tabstop = &tabstop
      let &tabstop=8
   endif

   " save cursorposition
   let save_pos = getcurpos()

   " saves current document to a temporary file
   silent w! %.emacsautotmp

   " runs the temporary file through emacs
   silent !emacs -batch -l ~/.emacs.d/lisp/verilog-mode.el %.emacsautotmp -f verilog-batch-auto

   " replaces current document with the emacs filtered temporary file
   silent %!cat %.emacsautotmp

   " restore cursor position
   silent call setpos('.', save_pos)

   " also replaces emacs generated tabs with spaces if expandtab is set
   if &expandtab
      retab
      let &tabstop=s:save_tabstop
   endif

   " removes temporary file
   silent !rm %.emacsautotmp
endfunction

" Delete function
" saves current document to a temporary file
" runs the temporary file through emacs
" replaces current document with the emacs filtered temporary file
" removes temporary file
function s:Delete()
   " save cursorposition
   let save_pos = getcurpos()

   " saves current document to a temporary file
   silent w! %.emacsautotmp

   " runs the temporary file through emacs
   silent !emacs -batch -l ~/.emacs.d/lisp/verilog-mode.el %.emacsautotmp -f verilog-batch-delete-auto

   " replaces current document with the emacs filtered temporary file
   silent %!cat %.emacsautotmp

   " restore cursor position
   silent call setpos('.', save_pos)

   " removes temporary file
   silent !rm %.emacsautotmp
endfunction

" Inject function
function s:Inject()
   if &expandtab
      let s:save_tabstop = &tabstop
      let &tabstop=8
   endif

   " save cursorposition
   let save_pos = getcurpos()

   " saves current document to a temporary file
   silent w! %.emacsautotmp

   " runs the temporary file through emacs
   silent !emacs -batch -l ~/.emacs.d/lisp/verilog-mode.el %.emacsautotmp -f verilog-batch-inject-auto

   " replaces current document with the emacs filtered temporary file
   silent %!cat %.emacsautotmp

   " restore cursor position
   silent call setpos('.', save_pos)

   if &expandtab
      retab
      let &tabstop=s:save_tabstop
   endif

   " removes temporary file
   silent !rm %.emacsautotmp
endfunction

" Indent function
function s:Indent()
   if &expandtab
      let s:save_tabstop = &tabstop
      let &tabstop=8
   endif

   " save cursorposition
   let save_pos = getcurpos()

   " saves current document to a temporary file
   silent w! %.emacsautotmp

   " runs the temporary file through emacs
   silent !emacs -batch -l ~/.emacs.d/lisp/verilog-mode.el %.emacsautotmp -f verilog-batch-indent-auto

   " replaces current document with the emacs filtered temporary file
   silent %!cat %.emacsautotmp

   " restore cursor position
   silent call setpos('.', save_pos)

   " also replaces emacs generated tabs with spaces if expandtab is set
   if &expandtab
      retab
      let &tabstop=s:save_tabstop
   endif

   " removes temporary file
   silent !rm %.emacsautotmp
endfunction
