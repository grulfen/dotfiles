" Plugins {{{
let g:ale_completion_enabled = 0
call plug#begin('~/.config/nvim/plugged')

Plug 'Vimjas/vim-python-pep8-indent'
Plug 'altercation/vim-colors-solarized'
Plug 'bronson/vim-trailing-whitespace'
Plug 'ellisonleao/gruvbox.nvim'
Plug 'folke/trouble.nvim'
Plug 'David-Kunz/gen.nvim'
Plug 'frazrepo/vim-rainbow'
Plug 'ggml-org/llama.vim'
Plug 'godlygeek/tabular'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-omni'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/nvim-cmp'
Plug 'junegunn/fzf', { 'dir': '~/source/fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'justinmk/vim-sneak'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'lewis6991/gitsigns.nvim'
Plug 'mfussenegger/nvim-dap'
Plug 'mfussenegger/nvim-dap-python'
Plug 'nanotech/jellybeans.vim'
Plug 'nathangrigg/vim-beancount'
Plug 'neovim/nvim-lspconfig'
Plug 'neovimhaskell/haskell-vim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-lualine/lualine.nvim'
Plug 'nvim-neotest/nvim-nio'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-tree/nvim-tree.lua'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-textobjects'
Plug 'rcarriga/nvim-dap-ui'
Plug 'sainnhe/everforest'
Plug 'sindrets/diffview.nvim'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tversteeg/registers.nvim'
Plug 'vim-test/vim-test'
Plug 'vimwiki/vimwiki'
Plug 'w0rp/ale'

call plug#end()
" }}}

" Color {{{
colorscheme everforest    " Pretty colors
let g:rainbow_active = 1

if has("termguicolors")
    set termguicolors
endif
" }}}

" Leader {{{
let mapleader = " "                       " space as leader
nnoremap <leader>ev :vsp    $MYVIMRC<cr>  " open init.vim in split
nnoremap <leader>sv :source $MYVIMRC<cr>  " source init.vim
nnoremap <leader>D :b#<bar>bd#<cr>

noremap <leader>f :Telescope git_files<CR>
noremap <leader>F :Telescope find_files<CR>
noremap <leader>l :Telescope find_files search_dirs=%:p:h<CR>
noremap <leader>b :Telescope buffers<CR>
noremap <leader>j :cnext<CR>
noremap <leader>k :cprev<CR>

noremap <leader>tf :NvimTreeFindFile<CR>
noremap <leader>tt :NvimTreeFindFileToggle<CR>
" }}}

" LSP and Linters {{{
let g:ale_fixers = {
\ '*':  ['remove_trailing_lines', 'trim_whitespace'],
\}
let g:ale_fix_on_save = 1

" LSP
lua << EOF
require'lspconfig'.hls.setup{}
require'lspconfig'.clangd.setup{
cmd = { "clangd", "--background-index", "--clang-tidy"}
}
require'lspconfig'.rust_analyzer.setup{}
require'lspconfig'.gopls.setup{}
require'lspconfig'.pylsp.setup{
settings = {
    pylsp = {
        plugins = {
            pylint = { enabled = false },
            mypy = { enabled = true },
            flake8 = { enabled = false },
            pycodestyle = { enabled = false },
            ruff = { enabled = true },
            isort = { enabled = true },
            },
        },
    },
}
EOF

nnoremap <silent> gd <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> gh <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> <leader>gr :Telescope lsp_references<CR>
nnoremap <silent> gr <cmd>lua vim.lsp.buf.rename()<CR>
nnoremap <silent> <leader>gk :Telescope lsp_document_symbols<CR>
nnoremap <silent> <leader>gw <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
nnoremap <silent> <leader>gg <cmd>lua vim.lsp.buf.code_action()<CR>
nnoremap <silent> <leader>hh <cmd>lua vim.diagnostic.open_float()<CR>
nnoremap <silent> <leader>gf <cmd>lua vim.lsp.buf.format()<CR>

" }}}

" Treesitter {{{
lua << EOF
require'nvim-treesitter.configs'.setup {
    ensure_installed = { "cpp", "c", "bash", "fish", "python", "lua", "vim", "vimdoc", "rust", "go" },
    highlight = {
        enable = true,
        disable = { "vimdoc", "help" },
    },
    textobjects = {
        select = {
            enable = true,
            keymaps = {
                ["af"] = "@function.outer",
                ["if"] = "@function.inner",
                ["ac"] = "@class.outer",
                ["ic"] = "@class.inner",
            },
            selection_modes = {
                ['@function.outer'] = 'V',
                ['@class.outer'] = 'V',
            },
        },
        swap = {
            enable = true,
            swap_next = {
                ["<leader>p"] = "@parameter.inner",
                ["<leader>a"] = "@argument.inner",
            },
            swap_previous = {
                ["<leader>P"] = "@parameter.inner",
                ["<leader>P"] = "@argument.inner",
            },
        },
        move = {
            enable = true,
            set_jumps = true, -- whether to set jumps in the jumplist
            goto_next_start = {
                ["]m"] = "@function.outer",
                ["]]"] = { query = "@class.outer", desc = "Next class start" },
            },
            goto_next_end = {
                ["]M"] = "@function.outer",
                ["]["] = "@class.outer",
            },
            goto_previous_start = {
                ["[m"] = "@function.outer",
                ["[["] = "@class.outer",
            },
            goto_previous_end = {
                ["[M"] = "@function.outer",
                ["[]"] = "@class.outer",
            },
        },
    },
}
EOF
" }}}

" nvim-tree {{{
lua << EOF
require('nvim-tree').setup {
    view = {
        adaptive_size = true,
    },
    git = {
        ignore = false,
    },
}
EOF
" }}}

" nvim-cmp {{{
lua << EOF
local cmp = require'cmp'

cmp.setup{
    sources = {
        { name = 'omni' },
        { name = 'nvim_lsp' },
        { name = 'buffer' },
    },
    mapping = {
        ['<CR>'] = cmp.mapping.confirm {
          behavior = cmp.ConfirmBehavior.Insert,
          select = true,
        },

        ['<C-n>'] = function(fallback)
          if not cmp.select_next_item() then
            if vim.bo.buftype ~= 'prompt' and has_words_before() then
              cmp.complete()
            else
              fallback()
            end
          end
        end,

        ['<C-p>'] = function(fallback)
          if not cmp.select_prev_item() then
            if vim.bo.buftype ~= 'prompt' and has_words_before() then
              cmp.complete()
            else
              fallback()
            end
          end
        end,
    },
}


EOF
" }}}

" registers.nvim {{{
lua << EOF
require("registers").setup(
    {
        show_empty = false,
        system_clipboard = false,
    }
)
EOF
" }}}

" nvim-dap {{{
lua << EOF
local dap = require("dap")
dap.adapters.lldb = {
    type = 'executable',
    command = '/run/current-system/sw/bin/lldb-vscode',
    name = 'lldb'
}
dap.configurations.cpp = {
    {
        name = 'Launch',
        type = 'lldb',
        request = 'launch',
        program = function()
            return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
        end,
        cwd = '${workspaceFolder}',
        stopOnEntry = false,
        args = {},
    },
}

vim.api.nvim_set_keymap("n", "<F5>",  ":lua require('dap').continue()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<F10>", ":lua require('dap').step_over()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<F11>", ":lua require('dap').step_into()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<F12>", ":lua require('dap').step_out()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>dd", ":lua require('dapui').toggle()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>db", ":lua require('dap').toggle_breakpoint()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>dr", ":lua require('dap').repl.open()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>dh", ":lua require('dap.ui.widgets').hover()<CR>", { noremap = true })
vim.keymap.set("n", "<leader>df", function()
    local widgets = require('dap.ui.widgets')
    widgets.centered_float(widgets.frames)
end)
vim.keymap.set("n", "<leader>ds", function()
    local widgets = require('dap.ui.widgets')
    widgets.centered_float(widgets.scopes)
end)

require('dap-python').setup('~/.venv/debugpy/bin/python')

require("dapui").setup()
EOF
" }}}

" gen.nvim {{{
lua << EOF
require('gen').setup({
    model = "llama3:latest",
    host = "localhost",
    port = "11434",
})
EOF
" }}}

" Misc {{{
syntax enable
set guicursor=          " fix error with nvim printing strange character on entering insert mode
set virtualedit=block   " Enable to move cursor on places without characters in visual block mode
" }}}

" Spaces and Tabs {{{
set tabstop=4           " number of visual spaces per TAB
set softtabstop=4       " number of spaces in tab when editing
set expandtab           " tabs are spaces
" }}}

" UI config {{{
syntax on
set number            " show line numbers
set cursorline        " highlight current line
filetype plugin indent on
set wildmenu          " visual autocomplete for command menu
set lazyredraw        " redraw only when we need to
set showmatch         " highlight matching [{()}]
set diffopt+=vertical " Open diff window in vertical split
set shiftwidth=4      " number of spaces to use on << and >>

lua << END
require('lualine').setup {
    options = {
        theme = 'everforest'
    }
}
require('gitsigns').setup()
END


" }}}

" Searching {{{
set incsearch      " search as characters are entered
set hlsearch       " highlight matches
set ignorecase     " Ignore case when searching
set smartcase      " Dont ignore case if typeing capital letters in search
if executable("rg")
    set grepprg=rg\ --vimgrep\ --no-heading
endif

nnoremap <leader>, :nohlsearch<cr>  " clear search highlight
" }}}

" Folding {{{
set foldenable          " enable folding
set foldlevelstart=10   " open most folds by default
set foldmethod=expr   " use indent as fold indicator
set foldexpr=nvim_treesitter#foldexpr()
" }}}

" Autocommands {{{
autocmd WinEnter * set relativenumber     " set relativenumber when entering window
autocmd WinLeave * set norelativenumber   " set norelativenumber when entering window
" }}}

" Remaps {{{
inoremap jk <Esc>
inoremap kj <Esc>

noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

noremap ]h :Gitsigns next_hunk<CR>
noremap [h :Gitsigns prev_hunk<CR>
noremap <leader>gh :Gitsigns preview_hunk<CR>
" }}}

" Indent {{{
set autoindent    " Copy indentation from current line on <cr>
" }}}

" vim: set foldmethod=marker:foldlevel=0
