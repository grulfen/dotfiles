direnv hook fish | source
# ghcup-env
set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME
test -f /home/grulfen/.ghcup/env ; and set -gx PATH $HOME/.cabal/bin /home/grulfen/.ghcup/bin $PATH
