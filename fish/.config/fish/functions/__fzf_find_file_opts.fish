function __fzf_find_file_opts
	if test -d $argv[1]
tree -C $argv[1] | head -100
else
bat --color=always --theme=GitHub --style=header --line-range :100 --wrap=character $argv[1]
end
end
