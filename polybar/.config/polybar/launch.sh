#!/usr/bin/env bash

# Terminate already running bar instances
killall -q -r polybar

# Launch bar1 and bar2
echo "---" | tee -a /tmp/polybar_mybar.log
polybar mybar 2>&1 | tee -a /tmp/polybar_mybar.log & disown

echo "Bars launched..."
