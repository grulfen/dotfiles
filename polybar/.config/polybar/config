[global/wm]
include-file=~/.config/polybar/colors.ini

[bar-base]
type = bar
width = 100%
height = 18pt
radius = 0
dpi = 96
background = ${colors.bg0}
foreground = ${colors.fg}
line-size = 2pt
border-size = 3pt
border-color = ${colors.bg0}
padding-left = 0
padding-right = 0
module-margin = 0

; i3 stuff
wm-restack = i3

; tray
tray-position = right
tray-padding = 2

; fonts
font-0 = UbuntuMono Nerd Font:style=Normal:pixelsize=11
font-1 = UbuntuMono Nerd Font:style=Normal:pixelsize=14
font-2 = UbuntuMono Nerd Font:style=Normal:pixelsize=10:antialias=false

[bar/mybar]
inherit = bar-base

; monitor
monitor = DP-0

; position
bottom = false

; modules
modules-left = date time
modules-center = i3
modules-right = volume space cpu space space memory

[module/cpu]
type   = internal/cpu
format = <ramp-coreload>

interval = 0.3

ramp-coreload-spacing = 1
ramp-coreload-0-foreground = ${colors.green}
ramp-coreload-1-foreground = ${colors.green}
ramp-coreload-2-foreground = ${colors.yellow}
ramp-coreload-3-foreground = ${colors.yellow}
ramp-coreload-4-foreground = ${colors.orange}
ramp-coreload-5-foreground = ${colors.orange}
ramp-coreload-6-foreground = ${colors.red}
ramp-coreload-7-foreground = ${colors.red}
ramp-coreload-0 = ▁
ramp-coreload-1 = ▂
ramp-coreload-2 = ▃
ramp-coreload-3 = ▄
ramp-coreload-4 = ▅
ramp-coreload-5 = ▆
ramp-coreload-6 = ▇
ramp-coreload-7 = █

[module/memory]
type = internal/memory
interval = 3
format = <bar-used>
; Only applies if <bar-used> is used
bar-used-indicator =
bar-used-width = 20
bar-used-foreground-0 = ${colors.green}
bar-used-foreground-1 = ${colors.yellow}
bar-used-foreground-2 = ${colors.orange}
bar-used-foreground-3 = ${colors.red}
bar-used-fill = ▐
bar-used-empty = ▐
bar-used-empty-foreground = ${colors.bg2}

[module/date]
type = internal/date
format-background = ${colors.bg1}
format-padding = 1
format-margin = 0
interval = 1
date = %d %B
date-alt = %A, %d %B
label ="%date%"
label-foreground = ${colors.fg}
format-prefix-foreground = ${colors.blue}
format-prefix = "  "

[module/time]
type = internal/date
format-background = ${colors.bg1}
format-padding = 1
format-margin = 0
interval = 30
date = %H:%M
format-prefix = "  "
format-prefix-foreground = ${colors.blue}
label-foreground = ${colors.fg}
label = "%date%"

[module/i3]
type = internal/i3
format = <label-state> <label-mode>

label-mode = %mode%
label-mode-padding = 1
label-mode-background = ${colors.orange}
label-mode-foreground = ${colors.bg1}

label-focused = "%index%"
label-focused-foreground = ${colors.fg}
label-focused-background = ${colors.bg1}
label-focused-underline = ${colors.green}
label-focused-padding = 1

label-unfocused = "%index%"
label-unfocused-foreground = ${colors.fg}
label-unfocused-background = ${colors.bg1}
label-unfocused-padding = 1

label-urgent = "%index%"
label-urgent-foreground = ${colors.fg}
label-urgent-background = ${colors.orange}
label-urgent-padding = 1

[module/space]
type = custom/text
content = " "

[module/space-alt]
type = custom/text
content = " "
content-background = ${colors.bg1}
